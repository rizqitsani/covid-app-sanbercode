import React, { useEffect } from 'react';
import { Image, StatusBar, StyleSheet, View, Text } from 'react-native';

import colors from '../../styles/color';

const Splash = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Image
          source={require('../../assets/images/splash.png')}
          style={{ width: 300, height: 200 }}
          resizeMode={'contain'}
        />
        <Text style={styles.appName}>CovidApp</Text>
      </View>
      <View style={styles.sanberContainer}>
        <Text style={styles.sanberText}>supported by</Text>
        <Image
          source={require('../../assets/images/logo-sanber.png')}
          style={{ width: 120, height: 55 }}
          resizeMode={'contain'}
        />
      </View>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: StatusBar.currentHeight,
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 300,
  },
  appName: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 42,
    color: colors.primary,
  },
  sanberContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 25,
  },
  sanberText: {
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    color: colors.primary,
  },
});
