import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';

import globalStyles from '../../styles/global';
import colors from '../../styles/color';
import NationUpdate from '../../components/NationUpdate';

const useFetch = (url) => {
  const [isLoading, setIsLoading] = useState(true);
  const [total, setTotal] = useState({});
  const [penambahan, setPenambahan] = useState({});

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((response) => {
        setTotal(response.update.total);
        setPenambahan(response.update.penambahan);
        setIsLoading(false);
      });
  }, []);

  return { isLoading, total, penambahan };
};

const Home = ({ navigation }) => {
  const { isLoading, total, penambahan } = useFetch(
    'https://data.covid19.go.id/public/api/update.json'
  );

  return (
    <View style={globalStyles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.headerContainer}>
          <View style={styles.leftHeader}>
            <Text style={[globalStyles.heading3, { color: colors.black }]}>
              Data Pasien
            </Text>
            <Text style={[globalStyles.heading1, { color: colors.black }]}>
              Indonesia
            </Text>
            <Text style={[globalStyles.detail, { color: colors.secondary }]}>
              Minggu, 20 September 2020
            </Text>
          </View>
          <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
            <MaterialCommunityIcons
              name='menu'
              size={30}
              color={colors.black}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() =>
            WebBrowser.openBrowserAsync('https://kawalcovid19.id/faq')
          }
        >
          <View style={styles.factContainer}>
            <Text style={[globalStyles.heading2, { color: colors.white }]}>
              {'Kenali Virusnya >'}
            </Text>
            <Text
              style={[
                globalStyles.detail,
                { color: colors.white, fontSize: 10, marginTop: 3 },
              ]}
            >
              Virus korona adalah sebutan untuk jenis virus yang dapat
              menyebabkan penyakit pada hewan dan manusia. Disebut korona karena
              bentuknya yang seperti mahkota
            </Text>
          </View>
        </TouchableOpacity>
        <View style={styles.updateHeader}>
          <View style={styles.updateTitle}>
            <Text style={[globalStyles.heading2, { color: colors.black }]}>
              Update Kasus Corona
            </Text>
            <Text style={[globalStyles.detail, { color: colors.secondary }]}>
              Terakhir diupdate {penambahan.tanggal}
            </Text>
          </View>
          <TouchableOpacity onPress={() => navigation.push('DetailProv')}>
            <Text style={globalStyles.link}>Lihat Detail</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.updateMain}>
          <Text style={globalStyles.status}>Kasus Positif</Text>
          <Text style={[globalStyles.heading1, { color: colors.primary }]}>
            {total.jumlah_positif}
          </Text>
          <Text style={styles.addition}>(+{penambahan.jumlah_positif})</Text>
        </View>
        <View style={styles.updateDetail}>
          <NationUpdate
            status='Dirawat'
            amount={total.jumlah_dirawat}
            addition={penambahan.jumlah_dirawat}
            color={colors.yellow}
            icon='plus'
            iconBg='rgba(242, 201, 76, 0.1)'
          />
          <NationUpdate
            status='Sembuh'
            amount={total.jumlah_sembuh}
            addition={penambahan.jumlah_sembuh}
            color={colors.green}
            icon='heart'
            iconBg='rgba(27, 191, 129, 0.1)'
          />
          <NationUpdate
            status='Meninggal'
            amount={total.jumlah_meninggal}
            addition={penambahan.jumlah_meninggal}
            color={colors.red}
            icon='close'
            iconBg='rgba(254, 223, 205, 0.3)'
          />
        </View>
      </View>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  addition: {
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    color: colors.primary,
  },
  factContainer: {
    justifyContent: 'center',
    marginVertical: 15,
    padding: 20,
    backgroundColor: colors.primary,
    borderRadius: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  innerContainer: {
    flex: 1,
    padding: 25,
    backgroundColor: colors.white,
  },
  updateDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  updateHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  updateMain: {
    ...globalStyles.card,
    justifyContent: 'center',
    marginTop: 15,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
});
