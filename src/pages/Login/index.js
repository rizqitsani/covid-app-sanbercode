import React, { useContext, useState } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  KeyboardAvoidingView,
} from 'react-native';

import { AuthContext } from '../../context';
import globalStyles from '../../styles/global';
import colors from '../../styles/color';
import Users from '../../model/users';

const Login = ({ navigation }) => {
  const { signIn } = useContext(AuthContext);
  const [data, setData] = useState({
    NIK: '',
    password: '',
  });

  const idHandler = (val) => {
    setData({
      ...data,
      NIK: val,
    });
  };

  const passwordHandler = (val) => {
    setData({
      ...data,
      password: val,
    });
  };

  const loginHandler = (NIK, password) => {
    const validUser = Users.filter((user) => {
      return NIK == user.NIK && password == user.password;
    });

    if (data.NIK.length == 0 && data.password.length == 0) {
      Alert.alert('', 'NIK atau password tidak boleh kosong', [
        { text: 'MENGERTI' },
      ]);
      return;
    }

    if (validUser.length == 0) {
      Alert.alert('', 'NIK atau password yang Anda masukkan salah.', [
        { text: 'COBA LAGI' },
      ]);
      return;
    }

    signIn(validUser[0]);
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={globalStyles.container}>
        <View style={styles.innerContainer}>
          <KeyboardAvoidingView behavior='position'>
            <View style={styles.headerContainer}>
              <Text style={[globalStyles.heading1, { color: colors.white }]}>
                {'Welcome\nBack'}
              </Text>
            </View>
            <View style={styles.loginContainer}>
              <Text
                style={{
                  fontFamily: 'Poppins-Medium',
                  fontSize: 18,
                  color: colors.white,
                }}
              >
                NIK
              </Text>
              <TextInput
                style={styles.formInput}
                placeholder={'Masukkan Nomor NIK Anda'}
                onChangeText={(val) => idHandler(val)}
              />
              <Text
                style={{
                  fontFamily: 'Poppins-Medium',
                  fontSize: 18,
                  color: colors.white,
                }}
              >
                Password
              </Text>
              <TextInput
                style={styles.formInput}
                secureTextEntry={true}
                placeholder={'Masukkan password'}
                onChangeText={(val) => passwordHandler(val)}
              />
            </View>
          </KeyboardAvoidingView>
          <TouchableOpacity
            style={styles.loginButton}
            onPress={() => loginHandler(data.NIK, data.password)}
          >
            <Text
              style={{
                fontFamily: 'Poppins-SemiBold',
                fontSize: 24,
                color: colors.white,
              }}
            >
              {'Login >'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Login;

const styles = StyleSheet.create({
  innerContainer: {
    flex: 1,
    paddingVertical: 100,
    paddingHorizontal: 40,
    backgroundColor: colors.primary,
  },
  loginContainer: {
    marginTop: 60,
  },
  formInput: {
    fontFamily: 'Poppins-Light',
    fontSize: 14,
    marginBottom: 15,
    borderColor: colors.white,
    borderBottomWidth: 0.3,
    color: colors.white,
  },
  loginButton: {
    marginTop: 70,
  },
});
