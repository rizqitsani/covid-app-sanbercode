import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';

import globalStyles from '../../styles/global';
import colors from '../../styles/color';

const About = ({ navigation }) => {
  return (
    <View style={globalStyles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.headerContainer}>
          <Text style={[globalStyles.heading1, { color: colors.white }]}>
            About
          </Text>
          <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
            <MaterialCommunityIcons
              name='menu'
              size={30}
              color={colors.white}
            />
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 90 }}>
          <Text style={styles.content}>
            My name is Muhammad Rizqi Tsani. I’m a student in Informatics
            Department of Institut Teknologi Sepuluh Nopember Surabaya. This is
            my first project using react native.{' '}
            <Text
              style={{ fontWeight: 'bold' }}
              onPress={() => {
                WebBrowser.openBrowserAsync('https://t.me/rizqitsani');
              }}
            >
              Feel free to contact me!
            </Text>
          </Text>
        </View>
        <View style={{ marginTop: 60 }}>
          <Text style={styles.content}>
            <Text style={{ fontFamily: 'Poppins-Medium' }}>API Source:</Text>
            {'\ndata.covid19.go.id'}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default About;

const styles = StyleSheet.create({
  innerContainer: {
    flex: 1,
    paddingVertical: 60,
    paddingHorizontal: 40,
    backgroundColor: colors.primary,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content: {
    fontFamily: 'Poppins-Light',
    fontSize: 14,
    color: colors.white,
  },
});
