import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

import globalStyles from '../../styles/global';
import colors from '../../styles/color';

import Annotation from '../../components/Annotation';
import RegionUpdate from '../../components/RegionUpdate';

const useFetch = (url) => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [lastUpdate, setLastUpdate] = useState('');

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((response) => {
        setData(response.list_data);
        setLastUpdate(response.last_date);
        setIsLoading(false);
      });
  }, []);

  return { isLoading, data, lastUpdate };
};

const DetailProv = () => {
  const { isLoading, data, lastUpdate } = useFetch(
    'https://data.covid19.go.id/public/api/prov.json'
  );

  return (
    <View style={[globalStyles.container, { backgroundColor: colors.primary }]}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>Data pasien per provinsi</Text>
        <Text style={styles.headerParagraph}>
          Terakhir diupdate {lastUpdate}
        </Text>
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.annotationContainer}>
          <Annotation status='Positif' color={colors.orange} />
          <Annotation status='Sembuh' color={colors.green} />
          <Annotation status='Meninggal' color={colors.red} />
        </View>
        {isLoading ? (
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <ActivityIndicator size='large' />
          </View>
        ) : (
          <FlatList
            data={data}
            renderItem={(region) => (
              <RegionUpdate region={region.item} isLoading={isLoading} />
            )}
            keyExtractor={(item) => item.key}
            style={{ marginTop: 20 }}
          />
        )}
      </View>
    </View>
  );
};

export default DetailProv;

const styles = StyleSheet.create({
  headerContainer: {
    margin: 25,
  },
  contentContainer: {
    flex: 1,
    padding: 25,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: colors.white,
  },
  header: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 22,
    color: colors.white,
  },
  headerParagraph: {
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    color: colors.white,
  },
  annotationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 15,
  },
});
