export default {
  primary: '#222b45',
  secondary: '#929292',
  black: '#000000',
  white: '#ffffff',
  red: '#FF3B3B',
  orange: '#FDA069',
  yellow: '#F2C94C',
  green: '#1BBF81',
};
