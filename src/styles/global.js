import { StatusBar } from 'react-native';

import colors from './color';

StatusBar.setBackgroundColor(colors.white);

export default {
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
  },
  card: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ececec',
  },
  heading1: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 36,
    lineHeight: 45,
  },
  heading2: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 18,
    lineHeight: 26,
  },
  heading3: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
  },
  amount: {
    fontFamily: 'Poppins-SemiBold',
    lineHeight: 25,
    fontSize: 22,
  },
  status: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    color: colors.secondary,
  },
  detail: {
    fontFamily: 'Poppins-Light',
    fontSize: 12,
  },
  link: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 12,
    color: '#007BEF',
  },
  addition: {
    fontFamily: 'Poppins-Light',
    fontSize: 9,
  },
};
