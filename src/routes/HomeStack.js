import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../pages/Home';
import DetailProvScreen from '../pages/DetailProv';

const HomeStack = createStackNavigator();

export default () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name='Home'
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name='DetailProv'
        component={DetailProvScreen}
        options={{ headerShown: false }}
      />
    </HomeStack.Navigator>
  );
};
