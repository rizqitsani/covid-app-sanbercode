import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import AboutScreen from '../pages/About';

import DrawerContent from '../components/DrawerContent';
import HomeStack from '../routes/HomeStack';

const HomeDrawer = createDrawerNavigator();

export default () => {
  return (
    <HomeDrawer.Navigator
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <HomeDrawer.Screen
        name='Home'
        component={HomeStack}
        options={{ headerShown: false }}
      />
      <HomeDrawer.Screen
        name='About'
        component={AboutScreen}
        options={{ headerShown: false }}
      />
    </HomeDrawer.Navigator>
  );
};
