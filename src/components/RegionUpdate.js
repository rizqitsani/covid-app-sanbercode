import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import globalStyles from '../styles/global';
import colors from '../styles/color';

const formatNumber = (num) => {
  var str = num.toString().split('.');
  if (str[0].length >= 3) {
    str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.');
  }
  if (str[1] && str[1].length >= 3) {
    str[1] = str[1].replace(/(\d{3})/g, '$1 ');
  }
  return str.join('.');
};

const RegionUpdate = ({ region, isLoading }) => {
  const positif = formatNumber(region.jumlah_kasus);
  const sembuh = formatNumber(region.jumlah_sembuh);
  const meninggal = formatNumber(region.jumlah_meninggal);

  return (
    <View style={styles.region}>
      <Text style={styles.regionTitle}>{region.key}</Text>
      <View style={styles.regionContainer}>
        <View style={styles.regionCase}>
          <Text style={[globalStyles.amount, { color: colors.orange }]}>
            {positif}
          </Text>
          <Text style={[globalStyles.addition, { color: colors.orange }]}>
            (+{region.penambahan.positif})
          </Text>
        </View>
        <View style={styles.regionCase}>
          <Text style={[globalStyles.amount, { color: colors.green }]}>
            {sembuh}
          </Text>
          <Text style={[globalStyles.addition, { color: colors.green }]}>
            (+{region.penambahan.sembuh})
          </Text>
        </View>
        <View style={styles.regionCase}>
          <Text style={[globalStyles.amount, { color: colors.red }]}>
            {meninggal}
          </Text>
          <Text style={[globalStyles.addition, { color: colors.red }]}>
            (+{region.penambahan.meninggal})
          </Text>
        </View>
      </View>
    </View>
  );
};

export default RegionUpdate;

const styles = StyleSheet.create({
  region: {
    marginBottom: 20,
  },
  regionTitle: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    marginBottom: 5,
    marginLeft: 5,
  },
  regionContainer: {
    ...globalStyles.card,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  regionCase: {
    alignItems: 'center',
  },
});
