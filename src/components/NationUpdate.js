import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { MaterialCommunityIcons } from '@expo/vector-icons';

import globalStyles from '../styles/global';

const NationUpdate = (props) => {
  return (
    <View style={styles.container}>
      <View style={[styles.iconContainer, { backgroundColor: props.iconBg }]}>
        <MaterialCommunityIcons
          name={props.icon}
          size={24}
          color={props.color}
        />
      </View>
      <View style={styles.content}>
        <Text style={[globalStyles.amount, { color: props.color }]}>
          {props.amount}
        </Text>
        <Text style={[globalStyles.addition, { color: props.color }]}>
          (+{props.addition})
        </Text>
        <Text style={[globalStyles.status, { marginTop: 5 }]}>
          {props.status}
        </Text>
      </View>
    </View>
  );
};

export default NationUpdate;

const styles = StyleSheet.create({
  container: {
    ...globalStyles.card,
    width: 100,
    alignItems: 'center',
    paddingVertical: 10,
    paddingVertical: 20,
  },
  content: {
    alignItems: 'center',
    marginTop: 25,
  },
  iconContainer: {
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
});
