import React, { useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { DrawerItem } from '@react-navigation/drawer';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { AuthContext } from '../context';
import globalStyles from '../styles/global';
import colors from '../styles/color';

const DrawerContent = (props) => {
  const { signOut } = useContext(AuthContext);

  return (
    <View style={globalStyles.container}>
      <View style={{ flex: 1 }}>
        <View style={styles.innerContainer}>
          <Text style={[globalStyles.heading1, { color: colors.primary }]}>
            CovidApp
          </Text>
        </View>
        <DrawerItem
          icon={({ color, size }) => (
            <MaterialCommunityIcons
              name='home-outline'
              color={color}
              size={size}
            />
          )}
          label='Home'
          onPress={() => {
            props.navigation.navigate('Home');
          }}
        />
        <DrawerItem
          icon={({ color, size }) => (
            <MaterialCommunityIcons
              name='information-outline'
              color={color}
              size={size}
            />
          )}
          label='About'
          onPress={() => {
            props.navigation.navigate('About');
          }}
        />
      </View>
      <View style={styles.bottomDrawer}>
        <DrawerItem
          icon={({ color, size }) => (
            <MaterialCommunityIcons
              name='exit-to-app'
              color={color}
              size={size}
            />
          )}
          label='Sign Out'
          onPress={() => {
            signOut();
          }}
        />
      </View>
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  innerContainer: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderColor: '#ececec',
    borderBottomWidth: 1,
  },
  bottomDrawer: {
    marginBottom: 5,
    borderTopColor: '#ececec',
    borderTopWidth: 1,
  },
});
