import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Annotation = (props) => {
  return (
    <View style={styles.annotation}>
      <View style={[styles.square, { backgroundColor: props.color }]} />
      <Text>{props.status}</Text>
    </View>
  );
};

export default Annotation;

const styles = StyleSheet.create({
  annotation: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  square: {
    height: 10,
    width: 10,
    marginRight: 3,
  },
});
