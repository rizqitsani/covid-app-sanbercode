import React, { useEffect, useMemo, useReducer, useState } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import { AppLoading } from 'expo';
import { useFonts } from 'expo-font';

import { AuthContext } from './src/context';
import HomeDrawer from './src/routes/HomeDrawer';
import SplashScreen from './src/pages/Splash';
import LoginScreen from './src/pages/Login';

export default function App() {
  const initialAuthState = {
    isLoading: true,
    userToken: null,
    userNIK: null,
  };

  const authReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          userToken: action.token,
          userNIK: action.id,
          isLoading: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          userToken: null,
          userNIK: null,
          isLoading: false,
        };
    }
  };

  const [authState, dispatch] = useReducer(authReducer, initialAuthState);

  let [isFontsLoaded] = useFonts({
    'Poppins-SemiBold': require('./src/assets/fonts/Poppins-SemiBold.ttf'),
    'Poppins-Medium': require('./src/assets/fonts/Poppins-Medium.ttf'),
    'Poppins-Light': require('./src/assets/fonts/Poppins-Light.ttf'),
  });

  const authFunctions = useMemo(() => {
    return {
      signIn: async (validUser) => {
        const userToken = String(validUser.userToken);
        try {
          userToken = 'qwerty';
          await AsyncStorage.setItem('userToken', userToken);
        } catch (error) {
          console.log(error);
        }
        dispatch({ type: 'LOGIN', token: userToken });
      },
      signOut: async () => {
        try {
          await AsyncStorage.removeItem('userToken');
        } catch (error) {
          console.log(error);
        }
        dispatch({ type: 'LOGOUT' });
      },
    };
  }, []);

  useEffect(() => {
    setTimeout(async () => {
      let userToken = null;
      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (error) {
        console.log(error);
      }
      dispatch({ type: 'RETRIEVE_TOKEN', token: userToken });
    }, 3000);
  }, []);

  if (!isFontsLoaded) {
    return <AppLoading />;
  }

  if (authState.isLoading) {
    return <SplashScreen />;
  }

  return (
    <AuthContext.Provider value={authFunctions}>
      <NavigationContainer>
        {authState.userToken ? <HomeDrawer /> : <LoginScreen />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
